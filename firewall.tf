resource "google_compute_firewall" "web" {
  name    = "firewall-nginx"
  network = "default"
  allow {
    protocol = "tcp"
    ports    = ["80", "8080", "1000-2000"]
  }

  source_ranges = ["0.0.0.0/0"]
}
